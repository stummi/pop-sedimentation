from contaminant import Contaminant
import helper
import matplotlib.pyplot as plt


DATA_DIR ='/media/ich/TOWER/m224013/model/output/ocean/PCBclim_cnt/'
WPOC = 10
DAYS_PER_MONTH = 30
KG_TO_MUG = 1e9


if __name__ == "__main__":
    pop = Contaminant(4, 'PCB-28')
    pop.load(DATA_DIR)
    sinking = pop.org_bottom * WPOC * DAYS_PER_MONTH * KG_TO_MUG
    sinking.name = f"sinking of {pop.name}"

    georef = helper.GeoRef(sinking.lon, sinking.lat)
    j, i = georef.get_indices(longitude=304, latitude=-63)

    sinking[:, j, i].groupby('time.year').sum().plot()
    plt.show()