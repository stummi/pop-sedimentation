import xarray as xr
import helper
import logging as log


class Contaminant:
    'a contaminant'

    def __init__(self, index, name):
        self.variables = dict(sinking=f"popsink{index}",
                              org=f"poporg{index}")
        self.pop = f"pop{index}"
        self.name = name
        self._ds = None

    def load(self, data_dir):
        file_name = f"{data_dir}/{self.pop}/{self.pop}_3d.????.nc"
        try:
            ds = xr.open_mfdataset(file_name, combine='by_coords')
            self._ds = helper.fix_time_axis(ds)
        except OSError as err:
            log.error("dataset not found %s" % file_name)
            log.error(err)

    @property
    def org_bottom(self) -> xr.DataArray:
        return self._ds[self.variables.get('org')].bottom_layer()

    @property
    def sink_bottom(self) -> xr.DataArray:
        return self._ds[self.variables.get('sinking')].bottom_layer()