import xarray as xr
import numpy as np


def fix_time_axis(ds):
    first_ts_as_str = str(ds.time[0].data)
    start_year = "%s-%s-%s" % (first_ts_as_str[0:4],
                               first_ts_as_str[4:6],
                               first_ts_as_str[6:8])
    ds['time'] = xr.cftime_range(start_year, freq="M", periods=len(ds['time'].data))
    return xr.decode_cf(ds)


@xr.register_dataarray_accessor('bottom_layer')
class BottomLayer(object):
    def __init__(self, xarray_obj):
        self._object = xarray_obj

    def __call__(self):
        return self._get_bottom_layer()

    def _get_bottom_layer(self) -> object:
        # reduces the array to the value of the last wet layer
        if 'depth' not in self._object.dims:
            raise AttributeError("dimension 'depth' not found in DataArray")

        rolled = self._object.roll(depth=-1, roll_coords=False)
        mask = (rolled * self._object).fillna(1)
        mask = mask.where(mask == 1, 0)
        return (self._object * mask).fillna(0).sum(dim="depth")


class GeoRef:

    def __init__(self, lon, lat):
        self.lons = lon
        self.lats = lat

    def get_indices(self, longitude=None, latitude=None, threshold=2):
        tmp = self.lons.where(np.abs(self.lons - longitude) < threshold)
        tmp = tmp.where(np.abs(self.lats - latitude) < threshold)
        return np.argwhere(~np.isnan(tmp.to_masked_array()))[0]
